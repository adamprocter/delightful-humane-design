# delightful humane design [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful) [![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://github.com/humanetech-community/awesome-humane-tech)


Humane design is a subset of humane technology and an upcoming field in information technology. This is a curated list of humane design resources for UX designers and developers.

## Contents

- [Principles](#principles)
- [Product design](#product-design)
- [User experience design](#user-experience-design)
- [Organizations](#organizations)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Principles

- [Principles for Technologists](https://www.humanetech.com/technologists#principles) - Principles for Humane Technology by the Center for Humane Technology.
- [Ethical Design Manifesto](https://2017.ind.ie/ethical-design/) - The Maslow's Hierarchy of Needs for sites and projects practicing ethical design.

## Product design

- [Design guide](https://www.humanetech.com/designguide) - Design products that are more sophisticated about human nature.

## User experience design

- [Humane by Design](https://humanebydesign.com/) - Guidance for designing ethically humane digital products through patterns focused on user well-being.

## Social media

Patterns and practices to improve social media and mitigate harmful effects on society.

- [De-Escalating Social Media](https://nickpunt.com/blog/deescalating-social-media/) - Designing humility and forgiveness into social media products.

## Organizations

Open communities and non-profits working in the field of humane technology.

- [Humane Tech Community](https://community.humanetech.com) - Promoting Solutions That Improve Wellbeing, Freedom and Society.
- [Center for Humane Technology](https://humanetech.com) - Envisioning a world where technology is realigned with humanity’s best interests.

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-humane-design/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)